/*
 * Copyright (C) 2004-2016 L2J DataPack
 * 
 * This file is part of L2J DataPack.
 * 
 * L2J DataPack is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J DataPack is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package quests.QC00001_CustomPaternKillCount;

import com.l2jserver.Config;
import com.l2jserver.gameserver.enums.QuestSound;
import com.l2jserver.gameserver.enums.QuestType;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.quest.Quest;
import com.l2jserver.gameserver.model.quest.QuestState;
import com.l2jserver.gameserver.model.quest.State;

/**
 * Cette quête est le model pour le type de "tue x mobs et raporte les items"
 * @author Rizaar
 */
public class QC00001_CustomPaternKillCount extends Quest
{
	// ID des npc utiles pour cette quête
	private static final int START_NPC = 50000; // TODO change me
	private static final int NPC_TO_SPEACK = 50000; // TODO change me
	private static final String NOM_DE_LA_QUETE = "nom de la test";
	private static final long XP_REWARD = 1L;
	private static final int SP_REWARD = 1;
	private static final int ID_QUETE = 2;
	private static final int[] ID_MOB_TO_KILL =
	{
		20514,
		20515
	};
	private static final int NB_MIN_ITEM_TO_COLLECT = 20; // TOTO change me
	
	// Misc conditions pour pouvoir contracter cette quête
	private static final int MIN_LEVEL = 2; // TODO change me
	// private static final int MAX_LEVEL = 10; //TODO uncoment if needed
	
	public QC00001_CustomPaternKillCount()
	{
		super(ID_QUETE, QC00001_CustomPaternKillCount.class.getSimpleName(), NOM_DE_LA_QUETE);
		addStartNpc(START_NPC);
		addTalkId(START_NPC, NPC_TO_SPEACK);
		addKillId(ID_MOB_TO_KILL);
	}
	
	/*
	 * Gère les événements déclanchés par la quête
	 * @see com.l2jserver.gameserver.model.quest.Quest#onAdvEvent(java.lang.String, com.l2jserver.gameserver.model.actor.L2Npc, com.l2jserver.gameserver.model.actor.instance.L2PcInstance)
	 */
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		final QuestState st = getQuestState(player, false);
		if (st == null)
		{
			return null;
		}
		
		String htmltext = null;
		switch (event)
		{
			case "goTo":
				_log.info("goTo");
				player.sendMessage("Vous vennez d'accepter la quête : " + NOM_DE_LA_QUETE);
				st.startQuest();
				st.setCond(1, false);
				htmltext = event + ".html"; // affichage du html correspondant
				break;
			case "end":
				_log.info("end");
				player.sendMessage("Vous vennez de complêter la quête : " + NOM_DE_LA_QUETE);
				st.exitQuest(QuestType.ONE_TIME, true);
				
				addExpAndSp(player, XP_REWARD * (long) Config.RATE_QUEST_DROP, SP_REWARD * (int) Config.RATE_QUEST_DROP);
				htmltext = "next.html"; // prochaine quête
				break;
		}
		return htmltext;
	}
	
	/**
	 * Gère l'evenement déclanché quand on dialogue avec un npc
	 */
	@Override
	public String onTalk(L2Npc npc, L2PcInstance player)
	{
		final QuestState st = getQuestState(player, true);
		String htmltext = getNoQuestMsg(player);
		switch (st.getState())
		{
			case State.CREATED:
			{
				_log.info("create");
				htmltext = (player.getLevel() < MIN_LEVEL) ? "tooLow.html" : "start.html";
				break;
			}
			case State.STARTED:
			{
				_log.info("started");
				if ((npc.getId() == START_NPC))
				{
					_log.info("notEnough");
					htmltext = "notEnough.html";
				}
				else if ((npc.getId() == NPC_TO_SPEACK) && ((st.getInt("20514") >= NB_MIN_ITEM_TO_COLLECT) && (st.getInt("20515") >= NB_MIN_ITEM_TO_COLLECT)))
				{
					_log.info("startedEnd");
					htmltext = "end.html";
				}
				break;
			}
			case State.COMPLETED:
			{
				_log.info("done");
				htmltext = "done.html";
				break;
			}
		}
		return htmltext;
	}
	
	@Override
	public String onKill(L2Npc npc, L2PcInstance killer, boolean isSummon)
	{
		L2PcInstance partyMember = getRandomPartyMember(killer, 1);
		if (partyMember == null)
		{
			partyMember = killer;
		}
		
		final QuestState st = getQuestState(partyMember, false);
		st.playSound(QuestSound.ITEMSOUND_QUEST_ITEMGET);
		switch (npc.getId())
		{
			case 20514:
				st.set("20514", st.getInt("20514") < NB_MIN_ITEM_TO_COLLECT ? st.getInt("20514") + 1 : NB_MIN_ITEM_TO_COLLECT);
				break;
			case 20515:
				st.set("20515", st.getInt("20515") < NB_MIN_ITEM_TO_COLLECT ? st.getInt("20515") + 1 : NB_MIN_ITEM_TO_COLLECT);
				break;
		}
		return super.onKill(npc, killer, isSummon);
	}
}
