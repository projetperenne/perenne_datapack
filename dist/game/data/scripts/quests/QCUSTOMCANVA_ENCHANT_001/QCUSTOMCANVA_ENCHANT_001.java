/**
 * Canva for Quest rewarding Crystal /EWS/ EAS for number of items get on mobs
 * @author jibe
 * name of variable to change :
 * QCUSTOMCANVA_ENCHANT_001 (ctrl+g replace)
 *
 * id to change :
 * NPCSTARTER
 * ENCHANT_WEAPON
 * CRYSTAL
 * MOBS_TAG
 * QUEST_ITEM
 *
 * string to change :
 * "Canva enchant quest" by the quest name
 */
 
package quests.QCUSTOMCANVA_ENCHANT_001;

import java.util.HashMap;
import java.util.Map;

import com.l2jserver.Config;
import com.l2jserver.gameserver.enums.QuestSound;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.quest.Quest;
import com.l2jserver.gameserver.model.quest.QuestState;

public class QCUSTOMCANVA_ENCHANT_001 extends Quest
{
	// NPC
	private static final int NPCSTARTER = 31537;

	// Reward items ID
	private static final int ENCHANT_WEAPON = 959; //EWS
	private static final int ENCHANT_ARMOR = 960; //EAS
	private static final int CRYSTAL = 9553; //WATER CRY

	// Mob ID
	private static final Map<Integer, Integer> MOBS_TAG = new HashMap<>();

	static
	{
		//usage :
		// MOBS_TAG.put(Id_mob, quest_item_drop_rate);
		MOBS_TAG.put(18905, 1000); // Farm Ravager (Crazy)
		MOBS_TAG.put(18906, 850); // Farm Bandit
		MOBS_TAG.put(18907, 850); // Beast Devourer
	}

	// Quest Item
	private static final int QUEST_ITEM = 15531; // SEL MAHUM MANE
	private static final int QUEST_ITEM_COUNT = 300; //number required of QUEST_ITEM

	// Misc
	private static final int MIN_LEVEL = 82; //min lvl to take the quest

	public QCUSTOMCANVA_ENCHANT_001()
	{
		super(278, QCUSTOMCANVA_ENCHANT_001.class.getSimpleName(), "Canva enchant quest");
		addStartNpc(NPCSTARTER);
		addTalkId(NPCSTARTER);
		addKillId(MOBS_TAG.keySet());
		registerQuestItems(QUEST_ITEM);
	}

	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		String htmltext = event;
		final QuestState st = getQuestState(player, false);
		if (st == null)
		{
			return htmltext;
		}

		switch (event)
		{
			case "31537-02.htm":
			{
				htmltext = (player.getLevel() >= MIN_LEVEL) ? "31537-02.htm" : "31537-03.html";
				break;
			}
			case "31537-04.htm":
			{
				st.startQuest();
				break;
			}
			case "31537-07.html":
			{
				if (st.isCond(1))
				{
					st.takeItems(QUEST_ITEM, QUEST_ITEM_COUNT);
					switch (getRandom(10))
					{
						case 0:
						{
							st.rewardItems(ENCHANT_WEAPON, 1);
							break;
						}
						case 1:
						case 2:
						case 3:
						{
							st.rewardItems(ENCHANT_ARMOR, 1);
							break;
						}
						case 4:
						case 5:
						{
							st.rewardItems(ENCHANT_ARMOR, 2);
							break;
						}
						case 6:
						{
							st.rewardItems(ENCHANT_ARMOR, 3);
							break;
						}
						case 7:
						case 8:
						{
							st.rewardItems(CRYSTAL, 1);
							break;
						}
						case 9:
						case 10:
						{
							st.rewardItems(CRYSTAL, 2);
							break;
						}
					}
					htmltext = event;
				}
				break;
			}
		}
		return htmltext;
	}

	@Override
	public String onKill(L2Npc npc, L2PcInstance player, boolean isSummon)
	{
		final L2PcInstance partyMember = getRandomPartyMember(player, 1);
		if (partyMember == null)
		{
			return super.onKill(npc, player, isSummon);
		}

		final QuestState st = getQuestState(partyMember, false);
		int npcId = npc.getId();
		float chance = (MOBS_TAG.get(npcId) * Config.RATE_QUEST_DROP);
		if (getRandom(1000) < chance)
		{
			st.rewardItems(QUEST_ITEM, 1);
			st.playSound(QuestSound.ITEMSOUND_QUEST_ITEMGET);
		}
		return super.onKill(npc, player, isSummon);

	}

	@Override
	public String onTalk(L2Npc npc, L2PcInstance player)
	{
		String htmltext = getNoQuestMsg(player);
		final QuestState st = getQuestState(player, true);
		if (st == null)
		{
			return htmltext;
		}

		if (st.isCreated())
		{
			htmltext = "31537-01.htm";
		}
		else if (st.isStarted())
		{
			if (st.isCond(1))
			{
				htmltext = (st.getQuestItemsCount(QUEST_ITEM) >= QUEST_ITEM_COUNT) ? "31537-05.html" : "31537-06.html";

			}

		}
		return htmltext;
	}

}
