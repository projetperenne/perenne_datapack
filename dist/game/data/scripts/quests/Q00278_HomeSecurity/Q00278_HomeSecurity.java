/*
 * Copyright (C) 2004-2016 L2J DataPack
 * 
 * This file is part of L2J DataPack.
 * 
 * L2J DataPack is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J DataPack is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package quests.Q00278_HomeSecurity;

import java.util.HashMap;
import java.util.Map;

import com.l2jserver.Config;
import com.l2jserver.gameserver.enums.QuestSound;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.quest.Quest;
import com.l2jserver.gameserver.model.quest.QuestState;

/**
 * Home Security (278)
 * @author jibe
 */
public class Q00278_HomeSecurity extends Quest
{
	// NPC
	private static final int TUNATUN = 31537;
	
	// Reward items ID
	private static final int ENCHANT_WEAPON_S = 959;
	private static final int ENCHANT_ARMOR_S = 960;
	private static final int WATER_CRYSTAL = 9553;
	
	// Mob ID
	private static final Map<Integer, Integer> MOBS_TAG = new HashMap<>();
	
	static
	{
		MOBS_TAG.put(18905, 1000); // Farm Ravager (Crazy)
		MOBS_TAG.put(18906, 850); // Farm Bandit
		MOBS_TAG.put(18907, 850); // Beast Devourer
	}
	
	// Quest Item
	private static final int SEL_MAHUM_MANE = 15531;
	private static final int SEL_MAHUM_MANE_COUNT = 300;
	
	// Misc
	private static final int MIN_LEVEL = 82;
	
	public Q00278_HomeSecurity()
	{
		super(278, Q00278_HomeSecurity.class.getSimpleName(), "Home Security");
		addStartNpc(TUNATUN);
		addTalkId(TUNATUN);
		addKillId(MOBS_TAG.keySet());
		registerQuestItems(SEL_MAHUM_MANE);
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		String htmltext = event;
		final QuestState st = getQuestState(player, false);
		if (st == null)
		{
			return htmltext;
		}
		
		switch (event)
		{
			case "31537-02.htm":
			{
				htmltext = (player.getLevel() >= MIN_LEVEL) ? "31537-02.htm" : "31537-03.html";
				break;
			}
			case "31537-04.htm":
			{
				st.startQuest();
				break;
			}
			case "31537-07.html":
			{
				if (st.isCond(1))
				{
					st.takeItems(SEL_MAHUM_MANE, 300);
					switch (getRandom(10))
					{
						case 0:
						{
							st.rewardItems(ENCHANT_WEAPON_S, 1);
							break;
						}
						case 1:
						case 2:
						case 3:
						{
							st.rewardItems(ENCHANT_ARMOR_S, 1);
							break;
						}
						case 4:
						case 5:
						{
							st.rewardItems(ENCHANT_ARMOR_S, 2);
							break;
						}
						case 6:
						{
							st.rewardItems(ENCHANT_ARMOR_S, 3);
							break;
						}
						case 7:
						case 8:
						{
							st.rewardItems(WATER_CRYSTAL, 1);
							break;
						}
						case 9:
						case 10:
						{
							st.rewardItems(WATER_CRYSTAL, 2);
							break;
						}
					}
					htmltext = event;
				}
				break;
			}
		}
		return htmltext;
	}
	
	@Override
	public String onKill(L2Npc npc, L2PcInstance player, boolean isSummon)
	{
		final L2PcInstance partyMember = getRandomPartyMember(player, 1);
		if (partyMember == null)
		{
			return super.onKill(npc, player, isSummon);
		}
		
		final QuestState st = getQuestState(partyMember, false);
		int npcId = npc.getId();
		float chance = (MOBS_TAG.get(npcId) * Config.RATE_QUEST_DROP);
		if (getRandom(1000) < chance)
		{
			st.rewardItems(SEL_MAHUM_MANE, 1);
			st.playSound(QuestSound.ITEMSOUND_QUEST_ITEMGET);
		}
		return super.onKill(npc, player, isSummon);
		
	}
	
	@Override
	public String onTalk(L2Npc npc, L2PcInstance player)
	{
		String htmltext = getNoQuestMsg(player);
		final QuestState st = getQuestState(player, true);
		if (st == null)
		{
			return htmltext;
		}
		
		if (st.isCreated())
		{
			htmltext = "31537-01.htm";
		}
		else if (st.isStarted())
		{
			if (st.isCond(1))
			{
				htmltext = (st.getQuestItemsCount(SEL_MAHUM_MANE) >= SEL_MAHUM_MANE_COUNT) ? "31537-05.html" : "31537-06.html";
				
			}
			
		}
		return htmltext;
	}
	
}
