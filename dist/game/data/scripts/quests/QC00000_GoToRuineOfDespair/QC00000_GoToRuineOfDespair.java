/*
 * Copyright (C) 2004-2016 L2J DataPack
 * 
 * This file is part of L2J DataPack.
 * 
 * L2J DataPack is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J DataPack is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package quests.QC00000_GoToRuineOfDespair;

import com.l2jserver.gameserver.enums.QuestType;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.quest.Quest;
import com.l2jserver.gameserver.model.quest.QuestState;
import com.l2jserver.gameserver.model.quest.State;

/**
 * @author Rizaar
 */
public class QC00000_GoToRuineOfDespair extends Quest
{
	// ID des npc utiles pour cette quête
	private static final int START_NPC = 30332; // TODO change me
	private static final int NPC_TO_SPEACK = 50000; // TODO change me
	private static final String NOM_DE_LA_QUETE = "Go To Ruine Of Despair";
	private static final long XP_REWARD = 7000L;
	private static final int SP_REWARD = 70;
	private static final int ID_QUETE = 1; // TODO change me
	
	// Misc conditions pour pouvoir contracter cette quête
	private static final int MIN_LEVEL = 15; // TODO change me
	// private static final int MAX_LEVEL = 10; //TODO uncoment if needed
	
	public QC00000_GoToRuineOfDespair()
	{
		super(ID_QUETE, QC00000_GoToRuineOfDespair.class.getSimpleName(), NOM_DE_LA_QUETE);
		addStartNpc(START_NPC);
		addTalkId(START_NPC, NPC_TO_SPEACK);
	}
	
	/*
	 * Gère les événements déclanchés par la quête
	 * @see com.l2jserver.gameserver.model.quest.Quest#onAdvEvent(java.lang.String, com.l2jserver.gameserver.model.actor.L2Npc, com.l2jserver.gameserver.model.actor.instance.L2PcInstance)
	 */
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		final QuestState st = getQuestState(player, false);
		if (st == null)
		{
			return null;
		}
		
		String htmltext = null;
		switch (event)
		{
			case "goTo":
				_log.info("goTo");
				player.sendMessage("Vous vennez d'accepter la quête : " + NOM_DE_LA_QUETE);
				st.startQuest();
				htmltext = event + ".html"; // affichage du html correspondant
				break;
			case "end":
				_log.info("end");
				player.sendMessage("Vous vennez de complêter la quête : " + NOM_DE_LA_QUETE);
				st.exitQuest(QuestType.ONE_TIME, true);
				addExpAndSp(player, XP_REWARD, SP_REWARD);
				htmltext = "next.html"; // prochaine quête
				break;
		}
		return htmltext;
	}
	
	/**
	 * Gère l'evenement déclanché quand on dialogue avec un npc
	 */
	@Override
	public String onTalk(L2Npc npc, L2PcInstance player)
	{
		final QuestState st = getQuestState(player, true);
		String htmltext = getNoQuestMsg(player);
		switch (st.getState())
		{
			case State.CREATED:
			{
				_log.info("create");
				htmltext = (player.getLevel() < MIN_LEVEL) ? "tooLow.html" : "start.html";
				break;
			}
			case State.STARTED:
			{
				_log.info("started");
				if (npc.getId() == START_NPC)
				{
					_log.info("startedgoTo");
					htmltext = "goTo.html";
				}
				else if (npc.getId() == NPC_TO_SPEACK)
				{
					_log.info("startedEnd");
					htmltext = "end.html";
				}
				break;
			}
			case State.COMPLETED:
			{
				_log.info("done");
				htmltext = "done.html";
				break;
			}
		}
		return htmltext;
	}
}
