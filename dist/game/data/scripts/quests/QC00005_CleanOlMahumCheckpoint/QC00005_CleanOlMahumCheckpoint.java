/*
 * Copyright (C) 2004-2016 L2J DataPack
 *
 * This file is part of L2J DataPack.
 *
 * L2J DataPack is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * L2J DataPack is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package quests.QC00005_CleanOlMahumCheckpoint;

import com.l2jserver.Config;
import com.l2jserver.gameserver.enums.QuestSound;
import com.l2jserver.gameserver.enums.QuestType;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.quest.Quest;
import com.l2jserver.gameserver.model.quest.QuestState;
import com.l2jserver.gameserver.model.quest.State;

import quests.QC00004_GoToOlMahumCheckpoint.QC00004_GoToOlMahumCheckpoint;

/**
 * Cette quête est le model pour le type de "tue x mobs et raporte les items"
 * @author Rizaar
 */
public class QC00005_CleanOlMahumCheckpoint extends Quest
{
	// ID des npc utiles pour cette quête
	private static final int START_NPC = 30653; // TODO change me
	private static final int NPC_TO_SPEACK = 30653; // TODO change me
	private static final String NOM_DE_LA_QUETE = "CleanOlMahumCheckpoint";
	private static final long XP_REWARD = 100000L;
	private static final int SP_REWARD = 10000;
	private static final int ID_QUETE = 460; // TODO change me
	
	private static final int monster1 = 20058;
	private static final int monster2 = 20053;
	
	private static final int NB_MIN_ITEM_TO_COLLECT = 20; // TOTO change me
	
	// Misc conditions pour pouvoir contracter cette quête
	private static final int MIN_LEVEL = 20; // TODO change me
	// private static final int MAX_LEVEL = 10; //TODO uncoment if needed
	
	public QC00005_CleanOlMahumCheckpoint()
	{
		super(ID_QUETE, QC00005_CleanOlMahumCheckpoint.class.getSimpleName(), NOM_DE_LA_QUETE);
		addStartNpc(START_NPC);
		addTalkId(START_NPC, NPC_TO_SPEACK);
		addKillId(monster1, monster2);
	}
	
	/*
	 * Gère les événements déclanchés par la quête
	 * @see com.l2jserver.gameserver.model.quest.Quest#onAdvEvent(java.lang.String, com.l2jserver.gameserver.model.actor.L2Npc, com.l2jserver.gameserver.model.actor.instance.L2PcInstance)
	 */
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		final QuestState st = getQuestState(player, false);
		if (st == null)
		{
			return null;
		}
		
		String htmltext = null;
		switch (event)
		{
			case "goTo":
				_log.info("goTo");
				player.sendMessage("Vous vennez d'accepter la quête : " + NOM_DE_LA_QUETE);
				st.startQuest();
				st.setCond(1, false);
				st.set("monster1", 0);
				st.set("monster2", 0);
				htmltext = event + ".html"; // affichage du html correspondant
				break;
			case "end":
				_log.info("end");
				player.sendMessage("Vous vennez de complêter la quête : " + NOM_DE_LA_QUETE);
				st.exitQuest(QuestType.ONE_TIME, true);
				addExpAndSp(player, XP_REWARD * (long) Config.RATE_QUEST_DROP, SP_REWARD * (int) Config.RATE_QUEST_DROP);
				htmltext = "next.html"; // prochaine quête
				break;
		}
		return htmltext;
	}
	
	/**
	 * Gère l'evenement déclanché quand on dialogue avec un npc
	 */
	@Override
	public String onTalk(L2Npc npc, L2PcInstance player)
	{
		final QuestState st = getQuestState(player, true);
		String htmltext = getNoQuestMsg(player);
		QuestState qc0001 = player.getQuestState(QC00004_GoToOlMahumCheckpoint.class.getSimpleName());
		switch (st.getState())
		{
			case State.CREATED:
			{
				_log.info("create");
				htmltext = (player.getLevel() < MIN_LEVEL) ? "tooLow.html" : qc0001.isCompleted() == true ? "start.html" : "prerequis.html";
				break;
			}
			case State.STARTED:
			{
				_log.info("started");
				if ((npc.getId() == NPC_TO_SPEACK) && (st.getInt("monster2") >= NB_MIN_ITEM_TO_COLLECT) && (st.getInt("monster1") >= NB_MIN_ITEM_TO_COLLECT))
				{
					_log.info("startedEnd");
					htmltext = "end.html";
				}
				else if ((npc.getId() == START_NPC))
				{
					_log.info("notEnough");
					htmltext = "notEnough.html";
				}
				
				break;
			}
			case State.COMPLETED:
			{
				_log.info("done");
				htmltext = "done.html";
				break;
			}
		}
		return htmltext;
	}
	
	@Override
	public String onKill(L2Npc npc, L2PcInstance killer, boolean isSummon)
	{
		
		L2PcInstance partyMember = getRandomPartyMember(killer, 1);
		if (partyMember == null)
		{
			partyMember = killer;
		}
		final QuestState st = getQuestState(partyMember, false);
		
		switch (npc.getId())
		{
			case monster1:
				st.set("monster1", st.getInt("monster1") >= 20 ? 20 : st.getInt("monster1") + 1);
				if ((st.getInt("monster1") == 1) || (st.getInt("monster1") == 5) || (st.getInt("monster1") == 10) || (st.getInt("monster1") == 15) || (st.getInt("monster1") == 20))
				{
					showOnScreenMsg(killer, "Vous avez tué : " + st.getInt("monster1") + " Zombie Soldier", 4000);
				}
				break;
			case monster2:
				st.set("monster2", st.getInt("monster2") >= 20 ? 20 : st.getInt("monster2") + 1);
				if ((st.getInt("monster2") == 1) || (st.getInt("monster2") == 5) || (st.getInt("monster2") == 10) || (st.getInt("monster2") == 15) || (st.getInt("monster2") == 20))
				{
					showOnScreenMsg(killer, "Vous avez tué : " + (st.getInt("monster2")) + " Zombie Warrior", 4000);
				}
				break;
		}
		st.playSound(QuestSound.ITEMSOUND_QUEST_ITEMGET);
		return super.onKill(npc, killer, isSummon);
	}
}
