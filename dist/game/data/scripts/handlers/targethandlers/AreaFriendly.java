/*
 * Copyright (C) 2004-2016 L2J DataPack
 * 
 * This file is part of L2J DataPack.
 * 
 * L2J DataPack is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J DataPack is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.targethandlers;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import com.l2jserver.gameserver.GeoData;
import com.l2jserver.gameserver.handler.ITargetTypeHandler;
import com.l2jserver.gameserver.model.L2Object;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.actor.instance.L2SiegeFlagInstance;
import com.l2jserver.gameserver.model.skills.Skill;
import com.l2jserver.gameserver.model.skills.targets.L2TargetType;
import com.l2jserver.gameserver.model.zone.ZoneId;
import com.l2jserver.gameserver.network.SystemMessageId;

/**
 * Area Friendly target handler implementation.
 * @author Adry_85, Zoey76
 */
public class AreaFriendly implements ITargetTypeHandler
{
	
	@Override
	public L2Object[] getTargetList(Skill skill, L2Character activeChar, boolean onlyFirst, L2Character target)
	{
		L2PcInstance player = activeChar.getActingPlayer();
		
		if (!checkTarget(player, target) && (skill.getCastRange() >= 0))
		{
			player.sendPacket(SystemMessageId.TARGET_IS_INCORRECT);
			return EMPTY_TARGET_LIST;
		}
		
		if (onlyFirst)
		{
			return new L2Character[]
			{
				target
			};
		}
		
		if (player.getActingPlayer().isInOlympiadMode())
		{
			return new L2Character[]
			{
				player
			};
		}
		
		final List<L2Character> targetList = new LinkedList<>();
		if (target != null)
		{
			// Add target to target list.
			
			int limit = skill.getAffectLimit();
			int i = 0;
			List<L2Character> list = target.getKnownList().getKnownCharactersInRadius(skill.getAffectRange());
			List<L2Character> allyList = new ArrayList<>();
			for (L2Character c : list)
			{
				if ((c.getActingPlayer() != null) && c.getActingPlayer().isPlayer())
				{
					if (isAllyOfCaster(c, player))
					{
						allyList.add(c);
					}
				}
			}
			allyList.sort(new CharComparator());
			for (L2Character obj : allyList)
			{
				if (i >= limit)
				{
					break;
				}
				targetList.add(obj);
				i++;
			}
			if (targetList.contains(target))
			{
				L2Character perm1 = targetList.get(targetList.indexOf(target));
				int indexPerm1 = targetList.indexOf(target);
				L2Character perm2 = targetList.get(0);
				targetList.remove(0);
				targetList.add(0, perm1);
				targetList.remove(indexPerm1);
				targetList.add(indexPerm1, perm2);
				targetList.add(allyList.get(limit));
			}
			else
			{
				targetList.add(0, target);
			}
			
		}
		
		if (targetList.isEmpty())
		{
			return EMPTY_TARGET_LIST;
		}
		return targetList.toArray(new L2Character[targetList.size()]);
	}
	
	private boolean checkTarget(L2PcInstance activeChar, L2Character target)
	{
		if (!GeoData.getInstance().canSeeTarget(activeChar, target))
		{
			return false;
		}
		
		if ((target == null) || target.isAlikeDead() || target.isDoor() || (target instanceof L2SiegeFlagInstance) || target.isMonster())
		{
			return false;
		}
		
		// GMs and hidden creatures.
		if (target.isInvisible())
		{
			return false;
		}
		
		if (target.isPlayable())
		{
			L2PcInstance targetPlayer = target.getActingPlayer();
			
			if (targetPlayer.inObserverMode() || targetPlayer.isInOlympiadMode())
			{
				return false;
			}
			
			if (activeChar.isInDuelWith(target))
			{
				return false;
			}
			
			if (activeChar.isInPartyWith(target))
			{
				return true;
			}
			
			// Only siege allies.
			if (activeChar.isInSiege() && !activeChar.isOnSameSiegeSideWith(targetPlayer))
			{
				return false;
			}
			
			if (target.isInsideZone(ZoneId.PVP))
			{
				return false;
			}
			
			if (activeChar.isInClanWith(target) || activeChar.isInAllyWith(target) || activeChar.isInCommandChannelWith(target))
			{
				return true;
			}
			
			if ((targetPlayer.getPvpFlag() > 0) || (targetPlayer.getKarma() > 0))
			{
				return false;
			}
		}
		return true;
	}
	
	private boolean isAllyOfCaster(L2Character target, L2PcInstance caster)
	{
		if (inYourParty(target.getActingPlayer(), caster) || inYourClan(target.getActingPlayer(), caster) || (!pvp(target.getActingPlayer()) && !karma(target.getActingPlayer()) && !wartag(target.getActingPlayer(), caster)))
		{
			return true;
		}
		return false;
	}
	
	private boolean inYourParty(L2PcInstance target, L2PcInstance caster)
	{
		if (target.getParty() != null)
		{
			return target.getParty().containsPlayer(caster);
		}
		return false;
	}
	
	private boolean inYourClan(L2PcInstance target, L2PcInstance caster)
	{
		if (target.getClan() != null)
		{
			return target.getClan().isMember(caster.getId());
		}
		return false;
	}
	
	private boolean pvp(L2PcInstance target)
	{
		return target.getPvpFlag() > 0;
	}
	
	private boolean karma(L2PcInstance target)
	{
		return target.getKarma() > 0;
	}
	
	private boolean wartag(L2PcInstance target, L2PcInstance caster)
	{
		if ((target.getClan() != null) && (caster.getClan() != null))
		{
			return target.getClan().isAtWarWith(caster.getClan().getId());
		}
		return false;
	}
	
	public static class CharComparator implements Comparator<L2Character>
	{
		@Override
		public int compare(L2Character char1, L2Character char2)
		{
			return Double.compare((char1.getCurrentHp() / char1.getMaxHp()), (char2.getCurrentHp() / char2.getMaxHp()));
		}
		
	}
	
	@Override
	public Enum<L2TargetType> getTargetType()
	{
		return L2TargetType.AREA_FRIENDLY;
	}
}