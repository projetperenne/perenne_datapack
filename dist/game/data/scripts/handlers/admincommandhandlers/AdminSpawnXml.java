/*
 * Copyright (C) 2004-2016 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.Config;
import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.data.xml.impl.NpcData;
import com.l2jserver.gameserver.datatables.SpawnTable;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.instancemanager.RaidBossSpawnManager;
import com.l2jserver.gameserver.model.L2Object;
import com.l2jserver.gameserver.model.L2Spawn;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.actor.templates.L2NpcTemplate;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jserver.util.Rnd;

/**
 * @author Rizaar
 */
public class AdminSpawnXml implements IAdminCommandHandler
{
	private static final Logger LOG = LoggerFactory.getLogger(AdminSpawnXml.class);
	private static final String PATH_ADMIN_SpawnList = "data/html/admin/perenneCmd/spawnXml/spawnlist/";
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_spawnlist",
		"admin_spawnlist_create_file",
		"admin_spawnlist_close_file",
		"admin_spawnlist_spawn_group",
		"admin_spawnlist_spawn_group_close",
		"admin_spawnlist_AIData",
		"admin_spawnlist_AIData_close",
		"admin_spawnlist_disable_random_animation",
		"admin_spawnlist_disable_random_walk",
		"admin_spawnlist_add_npc",
		"admin_spawnlist_add_npc_id",
		"admin_spawnlist_help",
		"admin_spawnlist_save_id",
		"admin_spawnlist_save_random_walk",
		"admin_spawnlist_save_random_animation",
		"admin_spawnlist_save_groupe_name",
		"admin_spawnlist_save_respawn_delay",
		"admin_spawnlist_save_titre",
		"admin_spawnlist_add_stock_id",
		"admin_spawnlist_add_npc_range",
		"admin_reset_range_id",
		"admin_spawnlist_add_stock_id_nb",
		"admin_spawnlist_init_custom_variable",
		"admin_spawnlist_spawn_groupe",
		"admin_spawnlist_range"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance activeChar)
	{
		final StringTokenizer st = new StringTokenizer(command, " ");
		String actualCommand = st.nextToken();
		LOG.info(activeChar.getName() + " : " + command);
		String cmd = "";
		switch (actualCommand.toLowerCase())
		{
			case "admin_spawnlist_create_file":
				cmd = st.nextToken();
				createSpawnListFile(activeChar, cmd);
				saveTitre(activeChar, cmd);
				SpawnList(activeChar);
				break;
			case "admin_spawnlist_close_file":
				endFile(activeChar);
				SpawnList(activeChar);
				break;
			case "admin_spawnlist_init_custom_variable":
				activeChar.getPerennePlayer().addOrUpdateVariableTemporaire("groupeName", "1");
				break;
			case "admin_spawnlist_spawn_group":
				LOG.info(activeChar.getPerennePlayer().getVariableTemporaire("groupeName"));
				activeChar.getPerennePlayer().addOrUpdateVariableTemporaire("groupeName", "" + (Integer.parseInt(activeChar.getPerennePlayer().getVariableTemporaire("groupeName")) + 1));
				createSpawnGroup(activeChar);
				SpawnList(activeChar);
				break;
			case "admin_spawnlist_spawn_group_close":
				endSpawnGroup(activeChar);
				SpawnList(activeChar);
				break;
			case "admin_spawnlist_AIData":
				createSpawnAiData(activeChar);
				SpawnList(activeChar);
				break;
			case "admin_spawnlist_AIData_close":
				endSpawnAiData(activeChar);
				SpawnList(activeChar);
				break;
			case "admin_spawnlist_disable_random_animation":
				disableRandomAnimation(activeChar);
				SpawnList(activeChar);
				break;
			case "admin_spawnlist_disable_random_walk":
				disableRandomWalk(activeChar);
				SpawnList(activeChar);
				break;
			case "admin_spawnlist_add_npc":
				addNpc(activeChar);
				SpawnList(activeChar);
				break;
			case "admin_spawnlist_add_npc_range":
				LOG.info(activeChar.getPerennePlayer().getVariableTemporaire("SpawnListMonsterIdRange"));
				StringTokenizer idComplete = new StringTokenizer(activeChar.getPerennePlayer().getVariableTemporaire("SpawnListMonsterIdRange"), " ");
				Integer count = Integer.parseInt(activeChar.getPerennePlayer().getVariableTemporaire("CurrentCountToResetSpawn"));
				Integer total = Integer.parseInt(activeChar.getPerennePlayer().getVariableTemporaire("CountToResetSpawn"));
				if (count >= total)
				{
					count = 0;
					endSpawnGroup(activeChar);
					createSpawnGroup(activeChar);
				}
				String id = idComplete.nextToken();
				
				// LOG.info(id);
				addNpcId(activeChar, id);
				String newKey = "";
				while (idComplete.hasMoreTokens())
				{
					newKey += idComplete.nextToken() + " ";
				}
				newKey += id;
				count++;
				activeChar.getPerennePlayer().addOrUpdateVariableTemporaire("SpawnListMonsterIdRange", newKey);
				activeChar.getPerennePlayer().addOrUpdateVariableTemporaire("CurrentCountToResetSpawn", count.toString());
				SpawnList(activeChar);
				break;
			case "admin_spawnlist_add_npc_id":
				cmd = st.nextToken();
				addNpcId(activeChar, cmd);
				SpawnList(activeChar);
				break;
			case "admin_spawnlist_add_stock_id":
				String str = "";
				while (st.hasMoreTokens())
				{
					str += st.nextToken() + " ";
					LOG.info(cmd);
				}
				addRangeId(activeChar, str);
				SpawnList(activeChar);
				break;
			case "admin_spawnlist_add_stock_id_nb":
				String str2 = "";
				String newId = "";
				Integer r = 0;
				while (st.hasMoreTokens())
				{
					r++;
					str2 = st.nextToken();
					StringTokenizer sub = new StringTokenizer(str2, ":");
					String identifiant = sub.nextToken();
					LOG.info(identifiant + ":");
					String strngNb = sub.nextToken();
					LOG.info(strngNb + ":");
					strngNb.substring(0, strngNb.length() - 1);
					LOG.info(strngNb + ":");
					int nb = Integer.valueOf(strngNb);
					for (int i = 0; i < nb; i++)
					{
						newId += identifiant + " ";
					}
					addRangeId(activeChar, newId);
				}
				activeChar.getPerennePlayer().addOrUpdateVariableTemporaire("CountToResetSpawn", r.toString());
				addRangeId(activeChar, newId);
				SpawnList(activeChar);
				break;
			case "admin_spawnlist_help":
				ShowHtml(activeChar, PATH_ADMIN_SpawnList + "help.htm", false);
				break;
			case "admin_spawnlist_save_id":
				cmd = st.nextToken();
				saveId(activeChar, cmd);
				SpawnList(activeChar);
				break;
			case "admin_spawnlist_save_random_walk":
				cmd = st.nextToken();
				saveRandomWalk(activeChar, cmd);
				SpawnList(activeChar);
				break;
			case "admin_spawnlist_save_random_animation":
				cmd = st.nextToken();
				saveRandomAnimation(activeChar, cmd);
				SpawnList(activeChar);
				break;
			case "admin_spawnlist_save_groupe_name":
				cmd = st.nextToken();
				saveGroupName(activeChar, cmd);
				SpawnList(activeChar);
				break;
			case "admin_spawnlist_save_respawn_delay":
				cmd = st.nextToken();
				saveRespawnDelay(activeChar, cmd);
				SpawnList(activeChar);
				break;
			case "admin_spawnlist_save_titre":
				cmd = st.nextToken();
				saveTitre(activeChar, cmd);
				SpawnList(activeChar);
				break;
			case "admin_spawnlist":
				SpawnList(activeChar);
				break;
			case "admin_reset_range_id":
				resetRangeId(activeChar);
				SpawnList(activeChar);
				break;
			case "admin_spawnlist_range":
				activeChar.getPerennePlayer().addOrUpdateVariableTemporaire("SpawnListRange", cmd = st.nextToken());
				break;
			case "admin_spawnlist_spawn_groupe":
				// LOG.info(activeChar.getPerennePlayer().getVariableTemporaire("SpawnListMonsterIdRange"));
				StringTokenizer nb = new StringTokenizer(activeChar.getPerennePlayer().getVariableTemporaire("SpawnListMonsterIdRange"), " ");
				Integer total1 = nb.countTokens();
				createSpawnGroup(activeChar);
				// LOG.info("" + total1);
				for (int i = 0; i < total1; i++)
				{
					StringTokenizer idComplete1 = new StringTokenizer(activeChar.getPerennePlayer().getVariableTemporaire("SpawnListMonsterIdRange"), " ");
					String id1 = idComplete1.nextToken();
					addNpcId(activeChar, id1);
					String newKey1 = "";
					while (idComplete1.hasMoreTokens())
					{
						newKey1 += idComplete1.nextToken() + " ";
					}
					newKey1 += id1;
					// LOG.info(newKey1);
					activeChar.getPerennePlayer().addOrUpdateVariableTemporaire("SpawnListMonsterIdRange", newKey1);
				}
				endSpawnGroup(activeChar);
				break;
		}
		return false;
		
	}
	
	/**
	 * @param activeChar
	 * @param cmd
	 */
	private void addRangeId(L2PcInstance activeChar, String cmd)
	{
		activeChar.getPerennePlayer().addOrUpdateVariableTemporaire("SpawnListMonsterIdRange", cmd);
	}
	
	private void resetRangeId(L2PcInstance activeChar)
	{
		activeChar.getPerennePlayer().deletVariableTemporaire("SpawnListMonsterIdRange");
	}
	
	private void saveId(L2PcInstance activeChar, String id)
	{
		StoreVariableTemp(activeChar, "SpawnListMonsterId", id);
	}
	
	private void saveRandomWalk(L2PcInstance activeChar, String randomWalk)
	{
		StoreVariableTemp(activeChar, "SpawnListRandomWalk", randomWalk);
	}
	
	private void saveRandomAnimation(L2PcInstance activeChar, String randomAnimation)
	{
		StoreVariableTemp(activeChar, "SpawnListRandomAnimation", randomAnimation);
	}
	
	private void saveGroupName(L2PcInstance activeChar, String randomAnimation)
	{
		StoreVariableTemp(activeChar, "SpawnListGroupName", randomAnimation);
	}
	
	private void saveTitre(L2PcInstance activeChar, String titre)
	{
		StoreVariableTemp(activeChar, "SpawnListTitre", titre);
	}
	
	private void saveRespawnDelay(L2PcInstance activeChar, String RespawnDelay)
	{
		StoreVariableTemp(activeChar, "SpawnListRespawnDelay", RespawnDelay);
	}
	
	/**
	 * Créer un fichier xml et l'initialise au format d'un fichier de SpawnList
	 * @param activeChar
	 * @param titre
	 * @return true si le fichier est créé
	 */
	private boolean createSpawnListFile(L2PcInstance activeChar, String titre)
	{
		activeChar.getPerennePlayer().addOrUpdateVariableTemporaire("SpawnListTitre", titre);
		File fichier = new File("adminOutputCmd/spawnlist/" + titre);
		if (!fichier.exists())
		{
			try
			{
				fichier.createNewFile();
				initialiseFile(titre);
			}
			catch (IOException e)
			{
				LOG.error("impossible de créer le fichier " + titre + " :" + e.getMessage());
				return false;
			}
		}
		return true;
	}
	
	/**
	 * initialise le fichier passé en parametre au format d'un fichier de SpawnList
	 * @param name
	 * @return
	 */
	private boolean initialiseFile(String name)
	{
		// entête du fichier xml
		String init = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + "\t<list enabled=\"true\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"../xsd/SpawnList.xsd\">\r\n";
		
		return writeInFile(name, init);
	}
	
	/**
	 * Ferme le DOM du xml d'un fichier SpawnList
	 * @param activeChar
	 * @return
	 */
	private boolean endFile(L2PcInstance activeChar)
	{
		// entête du fichier xml
		String init = "</list>\r\n";
		String name = GetVariableTemp(activeChar, "SpawnListTitre");
		
		return writeInFile(name, init);
	}
	
	private boolean createSpawnGroup(L2PcInstance activeChar)
	{
		String name = GetVariableTemp(activeChar, "SpawnListTitre");
		String spawnGroupName = activeChar.getPerennePlayer().getVariableTemporaire("SpawnListGroupName") + activeChar.getPerennePlayer().getVariableTemporaire("groupeName");
		LOG.info(spawnGroupName);
		// entête du fichier xml
		String init = "\t\t<spawn name=\"" + spawnGroupName + "\">\r\n";
		
		return writeInFile(name, init);
	}
	
	private boolean endSpawnGroup(L2PcInstance activeChar)
	{
		String name = GetVariableTemp(activeChar, "SpawnListTitre");
		String init = "</spawn>\r\n";
		return writeInFile(name, init);
	}
	
	private boolean createSpawnAiData(L2PcInstance activeChar)
	{
		String name = GetVariableTemp(activeChar, "SpawnListTitre");
		// entête du fichier xml
		String init = "\t\t\t<AIData>\r\n";
		return writeInFile(name, init);
	}
	
	private boolean endSpawnAiData(L2PcInstance activeChar)
	{
		String name = GetVariableTemp(activeChar, "SpawnListTitre");
		String init = "\t\t\t</AIData>\r\n";
		return writeInFile(name, init);
	}
	
	private boolean disableRandomAnimation(L2PcInstance activeChar)
	{
		String name = GetVariableTemp(activeChar, "SpawnListTitre");
		String init = "\t\t\t\t<disableRandomAnimation>" + GetVariableTemp(activeChar, "SpawnListRandomAnimation") + "</disableRandomAnimation>\r\n";
		return writeInFile(name, init);
	}
	
	private boolean disableRandomWalk(L2PcInstance activeChar)
	{
		String name = GetVariableTemp(activeChar, "SpawnListTitre");
		String init = "\t\t\t\t<disableRandomWalk>" + GetVariableTemp(activeChar, "SpawnListRandomWalk") + "</disableRandomWalk> \r\n";
		return writeInFile(name, init);
	}
	
	private boolean addNpc(L2PcInstance activeChar)
	{
		String name = GetVariableTemp(activeChar, "SpawnListTitre");
		String init = "\t\t\t\t<npc id=\"" + GetVariableTemp(activeChar, "SpawnListMonsterId") + "\" x=\"" + activeChar.getLocation().getX() + "\" y=\"" + activeChar.getLocation().getY() + "\" z=\"" + activeChar.getLocation().getZ() + "\" heading=\"" + activeChar.getLocation().getHeading()
			+ "\" respawnDelay=\"" + GetVariableTemp(activeChar, "SpawnListRespawnDelay") + "\" />\r\n";
		spawnMonster(activeChar, GetVariableTemp(activeChar, "SpawnListMonsterId"), 0, 1, false);
		return writeInFile(name, init);
	}
	
	private boolean addNpcId(L2PcInstance activeChar, String str)
	{
		String name = GetVariableTemp(activeChar, "SpawnListTitre");
		String init = "\t\t\t\t<npc id=\"" + str + "\" x=\"" + activeChar.getLocation().getX() + (Integer.parseInt(activeChar.getPerennePlayer().getVariableTemporaire("SpawnListRange"))) + "\" y=\"" + activeChar.getLocation().getY()
			+ (Integer.parseInt(activeChar.getPerennePlayer().getVariableTemporaire("SpawnListRange"))) + "\" z=\"" + activeChar.getLocation().getZ() + "\" heading=\"" + activeChar.getLocation().getHeading() + "\" respawnDelay=\"" + GetVariableTemp(activeChar, "SpawnListRespawnDelay") + "\" />\r\n";
		spawnMonsterLoc(activeChar, str, 0, 1, false);
		return writeInFile(name, init);
	}
	
	private boolean writeInFile(String name, String init)
	{
		try
		{
			// Création du flux bufférisé sur un FileReader, immédiatement suivi par un
			// try/finally, ce qui permet de ne fermer le flux QUE s'il le reader
			// est correctement instancié (évite les NullPointerException)
			BufferedWriter buff = new BufferedWriter(new FileWriter("adminOutputCmd/spawnlist/" + name, true));
			try
			{
				buff.write(init); // écriture dans le fichier
			}
			finally
			{
				// dans tous les cas, on ferme nos flux
				buff.close();
			}
		}
		catch (IOException e)
		{
			// erreur de fermeture des flux
			LOG.error("impossible de fermer: " + name + e.getMessage());
			return false;
		}
		return true;
	}
	
	private String GetVariableTemp(L2PcInstance activeChar, String search)
	{
		return activeChar.getPerennePlayer().getVariableTemporaire(search);
	}
	
	private void StoreVariableTemp(L2PcInstance activeChar, String search, String toStore)
	{
		activeChar.getPerennePlayer().addOrUpdateVariableTemporaire(search, toStore);
	}
	
	private static void ShowHtml(L2PcInstance activeChar, String path, boolean reload)
	{
		File file = new File(Config.DATAPACK_ROOT, path);
		String content = HtmCache.getInstance().loadFile(file);
		final NpcHtmlMessage html = new NpcHtmlMessage();
		if (content != null)
		{
			html.setHtml(content);
		}
		else
		{
			html.setHtml("<html><body>My text is missing:<br>" + path + "</body></html>");
		}
		
		activeChar.sendPacket(html);
	}
	
	private void SpawnList(L2PcInstance activeChar)
	{
		if (activeChar.getPerennePlayer().getVariableTemporaire("SpawnListPage") == null)
		{
			activeChar.getPerennePlayer().addOrUpdateVariableTemporaire("SpawnListPage", "param");
		}
		// String prefix = activeChar.getPerennePlayer().getVariableTemporaire("SpawnListPage");
		File file = new File(Config.DATAPACK_ROOT, PATH_ADMIN_SpawnList + "variable.htm");
		String content = HtmCache.getInstance().loadFile(file);
		final NpcHtmlMessage html = new NpcHtmlMessage();
		if (activeChar.getPerennePlayer().getVariableTemporaire("SpawnListTitre") == null)
		{
			activeChar.getPerennePlayer().addOrUpdateVariableTemporaire("SpawnListTitre", "SpawnList");
		}
		LOG.info("AdminSpawnXml" + activeChar.getPerennePlayer().getVariableTemporaire("SpawnListTitre"));
		if (activeChar.getPerennePlayer().getVariableTemporaire("SpawnListNom") == null)
		{
			activeChar.getPerennePlayer().addOrUpdateVariableTemporaire("SpawnListNom", "SpawnList");
		}
		if (activeChar.getPerennePlayer().getVariableTemporaire("SpawnListDelay") == null)
		{
			activeChar.getPerennePlayer().addOrUpdateVariableTemporaire("SpawnListRespawnDelay", "60");
		}
		if (activeChar.getPerennePlayer().getVariableTemporaire("SpawnListRandomWalk") == null)
		{
			activeChar.getPerennePlayer().addOrUpdateVariableTemporaire("SpawnListRandomWalk", "false");
		}
		if (activeChar.getPerennePlayer().getVariableTemporaire("SpawnListRandomAnimation") == null)
		{
			activeChar.getPerennePlayer().addOrUpdateVariableTemporaire("SpawnListRandomAnimation", "false");
		}
		if (activeChar.getPerennePlayer().getVariableTemporaire("SpawnListMonsterId") == null)
		{
			activeChar.getPerennePlayer().addOrUpdateVariableTemporaire("SpawnListMonsterId", "15000");
		}
		if (activeChar.getPerennePlayer().getVariableTemporaire("SpawnListGroupName") == null)
		{
			activeChar.getPerennePlayer().addOrUpdateVariableTemporaire("SpawnListGroupName", "aaaa");
		}
		File dir = new File("adminOutputCmd/spawnlist/");
		String list = "";
		for (File f : dir.listFiles())
		{
			list += ";" + f.getName();
		}
		html.setHtml(content);
		html.replace("%valeurNomFichier%", GetVariableTemp(activeChar, "SpawnListTitre"));
		html.replace("%listFichierXml%", list);
		html.replace("%valeurIdMonstre%", GetVariableTemp(activeChar, "SpawnListMonsterId"));
		html.replace("%valeurGroupName%", GetVariableTemp(activeChar, "SpawnListGroupName"));
		html.replace("%valeurRandomAnimation%", GetVariableTemp(activeChar, "SpawnListRandomAnimation"));
		html.replace("%valeurRandomWalk%", GetVariableTemp(activeChar, "SpawnListRandomWalk"));
		html.replace("%valeurRespawnDelay%", GetVariableTemp(activeChar, "SpawnListRespawnDelay"));
		html.replace("%Titre%", "Panel Admin SpawnList");
		activeChar.sendPacket(html);
		
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
	
	private void spawnMonsterLoc(L2PcInstance activeChar, String monsterId, int respawnTime, int mobCount, boolean permanent)
	{
		L2Object target = activeChar.getTarget();
		Location tp = activeChar.getLocation();
		tp.setXYZ(tp.getX() + Rnd.get(Math.negateExact(Integer.parseInt(activeChar.getPerennePlayer().getVariableTemporaire("SpawnListRange"))), (Integer.parseInt(activeChar.getPerennePlayer().getVariableTemporaire("SpawnListRange")))), tp.getY()
			+ Rnd.get(Math.negateExact(Integer.parseInt(activeChar.getPerennePlayer().getVariableTemporaire("SpawnListRange"))), (Integer.parseInt(activeChar.getPerennePlayer().getVariableTemporaire("SpawnListRange")))), tp.getZ());
		L2NpcTemplate template;
		if (monsterId.matches("[0-9]*"))
		{
			// First parameter was an ID number
			template = NpcData.getInstance().getTemplate(Integer.parseInt(monsterId));
		}
		else
		{
			// First parameter wasn't just numbers so go by name not ID
			template = NpcData.getInstance().getTemplateByName(monsterId.replace('_', ' '));
		}
		
		try
		{
			final L2Spawn spawn = new L2Spawn(template);
			if (Config.SAVE_GMSPAWN_ON_CUSTOM)
			{
				spawn.setCustom(true);
			}
			spawn.setX(tp.getX());
			spawn.setY(tp.getY());
			spawn.setZ(tp.getZ());
			spawn.setAmount(mobCount);
			spawn.setHeading(tp.getHeading());
			spawn.setRespawnDelay(respawnTime);
			if (tp.getInstanceId() > 0)
			{
				spawn.setInstanceId(tp.getInstanceId());
				permanent = false;
			}
			else
			{
				spawn.setInstanceId(0);
			}
			// TODO add checks for GrandBossSpawnManager
			if (RaidBossSpawnManager.getInstance().isDefined(spawn.getId()))
			{
				// activeChar.sendMessage("You cannot spawn another instance of " + template.getName() + ".");
			}
			else
			{
				if (template.isType("L2RaidBoss"))
				{
					spawn.setRespawnMinDelay(43200);
					spawn.setRespawnMaxDelay(129600);
					RaidBossSpawnManager.getInstance().addNewSpawn(spawn, 0, template.getBaseHpMax(), template.getBaseMpMax(), permanent);
				}
				else
				{
					SpawnTable.getInstance().addNewSpawn(spawn, permanent);
					spawn.init();
				}
				if (!permanent)
				{
					spawn.stopRespawn();
				}
				activeChar.sendMessage("Created " + template.getName() + " on " + target.getObjectId());
			}
		}
		catch (Exception e)
		{
			activeChar.sendPacket(SystemMessageId.TARGET_CANT_FOUND);
		}
	}
	
	private void spawnMonster(L2PcInstance activeChar, String monsterId, int respawnTime, int mobCount, boolean permanent)
	{
		L2Object target = activeChar.getTarget();
		if (target == null)
		{
			target = activeChar;
		}
		
		L2NpcTemplate template;
		if (monsterId.matches("[0-9]*"))
		{
			// First parameter was an ID number
			template = NpcData.getInstance().getTemplate(Integer.parseInt(monsterId));
		}
		else
		{
			// First parameter wasn't just numbers so go by name not ID
			template = NpcData.getInstance().getTemplateByName(monsterId.replace('_', ' '));
		}
		
		try
		{
			final L2Spawn spawn = new L2Spawn(template);
			if (Config.SAVE_GMSPAWN_ON_CUSTOM)
			{
				spawn.setCustom(true);
			}
			spawn.setX(target.getX());
			spawn.setY(target.getY());
			spawn.setZ(target.getZ());
			spawn.setAmount(mobCount);
			spawn.setHeading(activeChar.getHeading());
			spawn.setRespawnDelay(respawnTime);
			if (activeChar.getInstanceId() > 0)
			{
				spawn.setInstanceId(activeChar.getInstanceId());
				permanent = false;
			}
			else
			{
				spawn.setInstanceId(0);
			}
			// TODO add checks for GrandBossSpawnManager
			if (RaidBossSpawnManager.getInstance().isDefined(spawn.getId()))
			{
				activeChar.sendMessage("You cannot spawn another instance of " + template.getName() + ".");
			}
			else
			{
				if (template.isType("L2RaidBoss"))
				{
					spawn.setRespawnMinDelay(43200);
					spawn.setRespawnMaxDelay(129600);
					RaidBossSpawnManager.getInstance().addNewSpawn(spawn, 0, template.getBaseHpMax(), template.getBaseMpMax(), permanent);
				}
				else
				{
					SpawnTable.getInstance().addNewSpawn(spawn, permanent);
					spawn.init();
				}
				if (!permanent)
				{
					spawn.stopRespawn();
				}
				activeChar.sendMessage("Created " + template.getName() + " on " + target.getObjectId());
			}
		}
		catch (Exception e)
		{
			activeChar.sendPacket(SystemMessageId.TARGET_CANT_FOUND);
		}
	}
	
}