/*
 * Copyright (C) 2004-2016 L2J DataPack
 * 
 * This file is part of L2J DataPack.
 * 
 * L2J DataPack is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J DataPack is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.bypasshandlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.handler.IBypassHandler;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2ClassMasterInstance;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.quest.QuestState;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;
import com.perenneCustom.PerennePlayerStore;

import quests.QC00003_BossOfRuineOfDespair.QC00003_BossOfRuineOfDespair;

public class ClassQuest implements IBypassHandler
{
	private static final Logger LOG = LoggerFactory.getLogger(PerennePlayerStore.class);
	private static final String[] COMMANDS =
	{
		"firstClassRequest",
		"firstClassOk"
	};
	
	// Buffs
	
	@Override
	public boolean useBypass(String command, L2PcInstance activeChar, L2Character target)
	{
		if (!target.isNpc() || activeChar.isCursedWeaponEquipped())
		{
			return false;
		}
		
		if (command.equalsIgnoreCase(COMMANDS[0]))
		{
			checkFirstClassRequest(activeChar);
		}
		
		return true;
	}
	
	private void checkFirstClassRequest(L2PcInstance activeChar)
	{
		
		QuestState qc0003 = activeChar.getQuestState(QC00003_BossOfRuineOfDespair.class.getSimpleName());
		final int jobLevel = activeChar.getClassId().level();
		if (qc0003.isCompleted() && (jobLevel < 1))
		{
			LOG.info("test");
			L2ClassMasterInstance.showQuestionMark(activeChar);
		}
		else
		{
			final NpcHtmlMessage html = new NpcHtmlMessage(0);
			html.setHtml("<html><body>Sergent:<br> vous devez faire vos preuves. Revenez quand vous m'aurez aider.</body></html>");
			activeChar.sendPacket(html);
		}
	}
	
	@Override
	public String[] getBypassList()
	{
		return COMMANDS;
	}
}